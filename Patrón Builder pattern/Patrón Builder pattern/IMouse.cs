﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patrón_Builder_pattern
{
    interface IMouse
    {
        string Particularidad();
    }
    class Mouse_Alambrico: IMouse
    {
        public string Particularidad()
        {
            return "Teclado Alambrico";
        }
    }
    class Mouse_Inalambrico: IMouse
    {
        public string Particularidad()
        {
            return "Mouse Inalambrico";
        }
    }
}
