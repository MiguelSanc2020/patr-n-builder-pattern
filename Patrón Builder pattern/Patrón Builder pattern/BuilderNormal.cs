﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patrón_Builder_pattern
{
    class Constructor : Builder
    {
        //Aqui estamos creando las intancias de nuetro producto que tiene como nombre "COMPUTADORA"
        // Para esto la instancia "computadora" necesita que se adiciones ciertos elementos como son(C,M T, M )
        // Esto se da gracias a las especificaciones que nos dio nuestro director
        private Producto Computadora = new Producto();
        public void ConstrulleCpu()
        {
            //Este metodo crea una nueva instancia, ya sea que selecionemos Cual tipo de elementos.

            Computadora.InsertarCpu(new CpuGamaMedia());
        }
        public void ConstrulleMonitor()
        {
            Computadora.InsertarMonitor(new Monitor_Resolucion_Alta());
        }
        public void ConstrulleTeclado()
        {
            Computadora.InsertarTeclado(new Teclado_Profesional());
        }
        public void ConstrulleMouse()
        {
            Computadora.InsertarMouse(new Mouse_Inalambrico());
        }

        //Este metodo es sumamente importante, por que este nos va a permitir sacar el productor que hemos construido por el Builder
        // Esto es para que el exterio haga uso de el
        public Producto ObtenerProductor()
        { 
            return Computadora;
        }
    }
}
