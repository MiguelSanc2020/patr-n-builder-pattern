﻿using System;

namespace Patrón_Builder_pattern
{

    class Program
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine("********************Patrón de Diseño Builder Pattern************************");


            Director MiDirector = new Director();

            //Costruimos la Computadora
            //instanciamos nuestro constructor
            Constructor Pc = new Constructor();
            MiDirector.Construye(Pc);

            // Obtenemos el Producto
            Producto Computadora = Pc.ObtenerProductor();

            Computadora.mostrarComputadora();
            
            Console.WriteLine("Se Obtiene el Computador de Esctritorio");
        }
    }
}
