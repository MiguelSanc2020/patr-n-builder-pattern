﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patrón_Builder_pattern
{


    //El director se va encargar de indicar la secuencia al constructor para que valla creando aquello que es necesario 
    //La clase director va a contener un método que lo vamos a llamar "Construye"

    class Director
    {

        //este metodo está recibiendo como parametro algo que se está construyendo.
        // en otras palabras está reciendo como parametro un constructor en particular
        public void Construye (Builder pConstructor)
        {

            //Cada unos de estos métodos tan siendo implementado en la clase Constructor

            pConstructor.ConstrulleCpu();
            pConstructor.ConstrulleMonitor();
            pConstructor.ConstrulleTeclado();
            pConstructor.ConstrulleMouse();
        }
    }
}
